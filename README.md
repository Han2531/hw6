# Homework 6

#





[5pts] What is the name of the new file that the `zstd` command creates (via output redirection) ?
- rootfs.tar.zst

Check to be sure your system can find the `zstd` command: `which zstd`.
If the response is "zstd not found", then you'll have to install it `sudo apt install zstd`

CD back to `distros/lune` and run the README script:
`bash README`

This will download and prepare the prebuilt LuneOS image for us using our `config` variables and
the `extract2.sh` script.

When this step is complete, we should have an image of the full Lune root file system in a new file called `rootfs.tar.zst`

> [10pts] screenshot the output of `ls -alh` showing the new `rootfs.tar.zst` file (rootfs.png)





##### Try again to build the Multiboot image

Go back to the `pinephone-multi-boot` directory and try to run the image building script again:
`bash mkimage.sh`

This time, I got the error:

```
+ ../p-boot/.build/p-boot-conf-native . /dev/loop3p1
./mkimage-boot.sh: line 60: ../p-boot/.build/p-boot-conf-native: No such file or directory
```

In this case, the directory `../p-boot` is where the bootloader source code lives.  It seems like we might have to build the bootloader first. Let's go take a look there.










##### Look into the P-Boot bootloader:

(If you previously cloned the P-boot source code into a subdirectory within a VirtualBox shared folder, then you'll have to re-clone it somewhere else.  **IT WILL NOT WORK IF YOU MOVE IT**)

`cd ../p-boot`

Let's take a look at the `README` there.  According to the readme, we're going to need some tools

```
sudo apt install ninja-build php gcc-aarch64-linux-gnu
```

Now, we have to create the `config.ini` file based on the provided `config.ini.sample`.
First edit the `config.ini.sample` file so that it points to the cross-compiler we just
installed:

```
# config.ini.sample:
aarch64_prefix = aarch64-linux-gnu-
```

then rename the file: `mv config.ini.sample config.ini`

Attempt to build P-Boot bootloader:
```
php configure.php
ninja
```

It will probably fail with the message: 
```
/bin/sh: arm-none-eabi-gcc: command not found
```

Let's install the GCC tools for the ARM EABI:
```
sudo apt install gcc-arm-none-eabi
```

Try again to build P-Boot: `ninja`

If you did everything right, the build will conclude with the message:

```
[187/187] OBJDUMP .build/p-boot/bin.as

```

> [15 pts] screenshot the last few lines of successful build (pbootbuild.png)





##### Try again to build the Multiboot image


Go back to the `pinephone-multi-boot` directory and try to run the image building script again:
`bash mkimage.sh`

This time, the build failed with the message: 
```
ERROR: Can't resolve path './../builds/ppd-5.10/board-1.1.dtb
```

This is familiar... remember Megi's message in the multi-boot README where he said if you use a newer kernel, you'll have to update the scripts??  Well, it looks like a script is trying to find a file from the 'ppd-5.10' kernel build. We're using version 5.11.  Let's use Megi's suggested `sed` command to find and replace all references to "pp-5.10" with "pp-5.11".
Alternatively, you can just manually edit `mkimage-boot.sh` and replace the references by hand.

```
cd pinephone-multi-boot/
sed -i 's/ppd-5.10/pp-5.11/g' mkimage-boot.sh
```

(If you chose a different prebuilt kernel from Megi, you would use something other than "pp-5.11" in the above command)



##### Try again to build the Multiboot image


Go back to the `pinephone-multi-boot` directory and try to run the image building script again:
`bash mkimage.sh`


If all goes well, you'll see the following:

```
...

  0645f000-06853800 pboot2.argb /home/user/LuneImgBuild/pinephone-multi-boot/files/pboot2.argb

Total filesystem size 106830 KiB

+ losetup -d /dev/loop10
+ dd if=../p-boot/.build/p-boot.bin of=/tmp/multi.img bs=1024 seek=8 conv=notrunc
32+0 records in
32+0 records out
32768 bytes (33 kB, 32 KiB) copied, 0.00166012 s, 19.7 MB/s

```

and you will have an image at `/tmp/multi.img`.

> [15 pts] screencap the output of `ls -alh /tmp/multi.img` (multiimg.png)





##### Flash your fresh image to the SD card and boot the phone!!!

You've done this before.  In this case, if you're running the flashing utility on your host OS (eg. Win10), you'll have to first copy your image to your Virtual Box shared folder to get it into the host's file system:

`cp /tmp/multi.img /SHARED`
(in my case, /SHARED is a VB shared folder)


>[15 pts] Take a picture of your phone showing the main P-Boot Menu (bootmenu.jpg)
>[10 pts] Take a picture of your phone showing the luneOS desktop (desktop.jpg)
